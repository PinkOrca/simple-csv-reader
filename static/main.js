document.addEventListener('DOMContentLoaded', ev => {
  const form = document.querySelector('#header form');
  const files_input = document.querySelector('#csv_file');

  const preventDrag = (ev, val) => {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
    if (!!val) form.setAttribute('drag', 'true');
    else form.removeAttribute('drag');
  };

  form.addEventListener('dragover', ev => preventDrag(ev, true));
  form.addEventListener('dragleave', ev => preventDrag(ev, false));
  form.addEventListener('drop', ev => {
    preventDrag(ev, false);
    if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to access the file(s)
    const items = ev.dataTransfer.items;
    let files = [];
    Array.from(items).forEach(item => {
      if (item.kind === 'file') {
      const file = item.getAsFile();
      files.push(file);
      }
    });
    loadData(files[0]);
    } else {
    // Use DataTransfer interface to access the file(s)
    const files = Array.from(ev.dataTransfer.files);
    loadData(files[0]);
    };
  });

  form.addEventListener('change', ev => {
    if (files_input.files.length > 0) {
    const files = Array.from(files_input.files);
    loadData(files[0]);
    } else form.reset();
  });

  form.addEventListener('submit', ev => ev.preventDefault());
});


const loadData = file => {
  // Create a new FileReader
  let fileReader = new FileReader();

  // Define the function to be called when the file is read
  fileReader.onload = ev => {
    let csvData = ev.target.result;
    let rows = csvData.split("\n");
    let table = document.getElementById('table');

    // Clear the table
    table.innerHTML = '';
    // Loop through the rows and add them to the table
    for (let i = 0; i < rows.length; i++) {
      let cells = rows[i].split(",");
      let row = table.insertRow();

      for (let j = 0; j < cells.length; j++) {
        if (cells[j].trim(' ') == '') continue;
        let cell = row.insertCell();
        cell.textContent = cells[j];
      }
    }
  }

  // Read the file
  fileReader.readAsText(file);
}